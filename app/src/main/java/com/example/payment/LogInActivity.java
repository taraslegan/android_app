package com.example.payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.model.User;
import com.example.payment.user_registration.RegistrationActivity;
import com.example.payment.utilities_registration.UtilitiesActivity;

import io.realm.Realm;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {
    Button logIn;
    EditText userName;
    EditText password;
    TextInputLayout userNameLayout;
    TextInputLayout passwordLayout;
    TextView tvInvalidUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        logIn = findViewById(R.id.btn_log_in);
        userName = findViewById(R.id.user_name_log_in);
        password = findViewById(R.id.password_log_in);
        passwordLayout = findViewById(R.id.password_layout_log_in);
        userNameLayout = findViewById(R.id.user_name_layout_log_in);
        tvInvalidUser = findViewById(R.id.invalid_user);

        logIn.setOnClickListener(this);
    }


    public void signUpOnClick(View v) {
        Intent registration = new Intent(this, RegistrationActivity.class);
        startActivity(registration);
    }

    @Override
    public void onClick(View view) {
        Utils.hideKeyboard(this);

        String userNameValue = userName.getText().toString();
        String passwordValue = password.getText().toString();

        if (authenticateUser(userNameValue, passwordValue)) {
            tvInvalidUser.setVisibility(View.INVISIBLE);
            SaveSharedPreferences.setUserName(this, userNameValue);
            Intent intent = new Intent(this, UtilitiesActivity.class);
            startActivity(intent);
        } else {
            processUserAuthenticationError(userNameValue, passwordValue);
        }
    }

    private boolean authenticateUser(String userNameValue, String passwordValue) {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("user_name", userNameValue)
                .equalTo("password", passwordValue)
                .findFirst();
        realm.close();
        return user != null;
    }

    private void processUserAuthenticationError(String userNameValue, String passwordValue) {
        if ("".equals(userNameValue.trim()) && "".equals(passwordValue.trim())) {
            tvInvalidUser.setVisibility(View.INVISIBLE);
            userNameLayout.setErrorEnabled(true);
            userNameLayout.setError("Введіть ім'я!");
            passwordLayout.setErrorEnabled(true);
            passwordLayout.setError("Введіть пароль!");
        } else if ("".equals(userNameValue.trim())) {
            tvInvalidUser.setVisibility(View.INVISIBLE);
            userNameLayout.setErrorEnabled(true);
            userNameLayout.setError("Введіть ім'я!");
            passwordLayout.setErrorEnabled(false);
        } else if ("".equals(passwordValue.trim())) {
            tvInvalidUser.setVisibility(View.INVISIBLE);
            userNameLayout.setErrorEnabled(false);
            passwordLayout.setError("Введіть пароль!");
            passwordLayout.setErrorEnabled(true);
        } else {
            userNameLayout.setErrorEnabled(false);
            passwordLayout.setErrorEnabled(false);
            tvInvalidUser.setVisibility(View.VISIBLE);
        }
    }
}
