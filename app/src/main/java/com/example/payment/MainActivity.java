package com.example.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.payment.history.HistoryActivity;
import com.example.payment.tabs.UtilitiesFragmentAdapter;

import io.realm.Realm;

import static com.example.payment.SaveSharedPreferences.GAS_ID;
import static com.example.payment.SaveSharedPreferences.LIGHT_ID;
import static com.example.payment.SaveSharedPreferences.USER_NAME;
import static com.example.payment.SaveSharedPreferences.WATER_ID;

/**
 * Created by Тарас on 21.04.2018.
 */

public class MainActivity extends AppCompatActivity {

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        if (SaveSharedPreferences.getUserName(this).length() == 0) {
            Intent intent = new Intent(this, LogInActivity.class);
            startActivity(intent);
        } else {
            getSupportActionBar().setElevation(0);
            setContentView(R.layout.activity_main);
            ViewPager viewPager = findViewById(R.id.view_pager);
            initViewPager(viewPager);
            UtilitiesFragmentAdapter fragmentAdapter = new UtilitiesFragmentAdapter(this, getSupportFragmentManager());
            viewPager.setAdapter(fragmentAdapter);
            TabLayout tabLayout = findViewById(R.id.sliding_tabs);
            tabLayout.setElevation(10);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void initViewPager(ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Utils.hideKeyboard(MainActivity.this);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                SaveSharedPreferences.getSharedPreferences(this).edit().putString(USER_NAME, "").apply();
                SaveSharedPreferences.getSharedPreferences(this).edit().putString(GAS_ID, "").apply();
                SaveSharedPreferences.getSharedPreferences(this).edit().putString(WATER_ID, "").apply();
                SaveSharedPreferences.getSharedPreferences(this).edit().putString(LIGHT_ID, "").apply();
                Intent intent = new Intent(this, LogInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.history:
                Intent history = new Intent(this, HistoryActivity.class);
                startActivity(history);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
