package com.example.payment;

import android.app.Application;

import com.example.payment.db.DBInit;

import java.util.Date;

import io.realm.Realm;

/**
 * Created by Тарас on 28.04.2018.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Realm realm = Realm.getDefaultInstance();
        if (realm.isEmpty()) DBInit.fillDb();
    }
}
