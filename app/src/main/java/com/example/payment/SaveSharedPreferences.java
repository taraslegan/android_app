package com.example.payment;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Тарас on 5/20/2018.
 */

public class SaveSharedPreferences {
    public static final String USER_NAME = "user_name";
    public static final String GAS_ID = "gas_id";
    public static final String WATER_ID = "water_id";
    public static final String LIGHT_ID = "light_id";

    public static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserName(Context ctx, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(USER_NAME, "");
    }

    public static void setGasId(Context ctx, String gasId) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(GAS_ID, gasId);
        editor.apply();
    }

    public static String getGasId(Context ctx) {
        return getSharedPreferences(ctx).getString(GAS_ID, "");
    }

    public static void setWaterId(Context ctx, String gasId) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(WATER_ID, gasId);
        editor.apply();
    }

    public static String getWaterId(Context ctx) {
        return getSharedPreferences(ctx).getString(WATER_ID, "");
    }

    public static void setLightId(Context ctx, String gasId) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(LIGHT_ID, gasId);
        editor.apply();
    }

    public static String getLightId(Context ctx) {
        return getSharedPreferences(ctx).getString(LIGHT_ID, "");
    }
}
