package com.example.payment;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Тарас on 5/27/2018.
 */

public class Utils {

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    //only month and year
    public static Date formatDate(Date date) {
        SimpleDateFormat simpleDate = new SimpleDateFormat("MM-yyyy");
        String format = simpleDate.format(date);
        Date parsed = null;
        try {
            parsed = simpleDate.parse(format);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return parsed;
    }
}
