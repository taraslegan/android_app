package com.example.payment.custom_view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.lapism.searchview.SearchView;

/**
 * CustomSearchView was created because onClick method had issue.
 * If developer creates own voice click logic, it will be executed, and default onClick will be
 * executed too.
 */
public class CustomSearchView extends SearchView {

    @Override
    public void onClick(View v) {
        if (v == mBackImageView) {
            if (mSearchArrow != null) {
                close(true);
            } else {
                if (mOnMenuClickListener != null) {
                    mOnMenuClickListener.onMenuClick();
                }
            }
        } else if (v == mVoiceImageView) {
            if (mOnVoiceClickListener != null) {
                mOnVoiceClickListener.onVoiceClick();
            }
        } else if (v == mEmptyImageView) {
            if (mSearchEditText.length() > 0) {
                mSearchEditText.getText().clear();
            }
        } else if (v == mShadowView) {
            close(true);
        }
    }

    public CustomSearchView(Context context) {
        super(context);
    }

    public CustomSearchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSearchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
