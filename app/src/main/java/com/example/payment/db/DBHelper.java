package com.example.payment.db;

import android.app.Activity;

import com.example.payment.SaveSharedPreferences;
import com.example.payment.model.Account;
import com.example.payment.model.Payment;
import com.example.payment.model.Price;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Тарас on 6/3/2018.
 */

public class DBHelper {
    public static Account getAccount(Activity activity, String tabIdentifier) {
        long accountID = getAccountID(activity, tabIdentifier);
        Realm realm = Realm.getDefaultInstance();
        Account gasAccountToUpdate = realm.where(Account.class)
                .equalTo("ID_account", accountID)
                .findFirst();
        realm.close();
        return gasAccountToUpdate;
    }

    public static Date getLastPaidMonth(Activity activity, String tabIdentifier) {
        long accountID = getAccountID(activity, tabIdentifier);
        Realm realm = Realm.getDefaultInstance();
        Date date = realm.where(Payment.class)
                .equalTo("ID_account", accountID).maximumDate("date");
        realm.close();
        return date;
    }

    public static long getLastCounterData(Activity activity, String tabIdentifier) {
        long accountID = getAccountID(activity, tabIdentifier);
        Realm realm = Realm.getDefaultInstance();
        long lastCounterData = realm.where(Account.class)
                .equalTo("ID_account", accountID)
                .findFirst().getCounter_data();
        realm.close();
        return lastCounterData;
    }

    public static int getLastConsumedPerMonth(Activity activity, String tabIdentifier) {
        long accountID = getAccountID(activity, tabIdentifier);
        Realm realm = Realm.getDefaultInstance();
        int consumedPerMonth = realm.where(Payment.class)
                .equalTo("ID_account", accountID)
                .findAll().sort("date").last().getConsumed_per_month();
        realm.close();
        return consumedPerMonth;
    }

    private static long getAccountID(Activity activity, String tabIdentifier) {
        long accountID = 0;
        switch (tabIdentifier) {
            case "gas":
                accountID = Long.valueOf((SaveSharedPreferences.getGasId(activity)));
                break;
            case "light":
                accountID = Long.valueOf((SaveSharedPreferences.getLightId(activity)));
                break;
            case "water":
                accountID = Long.valueOf((SaveSharedPreferences.getWaterId(activity)));
                break;
        }
        return accountID;
    }

    public static String getMonth(int number) {
        Map<Integer, String> months = new HashMap<>();
        months.put(0, "січень");
        months.put(1, "лютий");
        months.put(2, "березень");
        months.put(3, "квітень");
        months.put(4, "травень");
        months.put(5, "червень");
        months.put(6, "липень");
        months.put(7, "серпень");
        months.put(8, "вересень");
        months.put(9, "жовтень");
        months.put(10, "листопад");
        months.put(11, "грудень");
        return months.get(number);
    }

    public static int getMonth(String month) {
        Map<Integer, String> months = new HashMap<>();
        months.put(0, "січень");
        months.put(1, "лютий");
        months.put(2, "березень");
        months.put(3, "квітень");
        months.put(4, "травень");
        months.put(5, "червень");
        months.put(6, "липень");
        months.put(7, "серпень");
        months.put(8, "вересень");
        months.put(9, "жовтень");
        months.put(10, "листопад");
        months.put(11, "грудень");

        int monthNumber = -1;
        for (int key : months.keySet()) {
            if (months.get(key).equals(month)) monthNumber = key;
        }
        return monthNumber;
    }

    public static HashMap<Integer, RealmList<Payment>> groupPaymentsByDate() {
        HashMap<Integer, RealmList<Payment>> groupByDate = new HashMap<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Payment> paymentsDistinctByDate = realm.where(Payment.class).distinct("date").sort("date", Sort.DESCENDING).findAll();
        int key = 0;
        for (Payment p : paymentsDistinctByDate) {
            RealmResults<Payment> payments = realm.where(Payment.class).equalTo("date", p.getDate()).findAll();
            RealmList<Payment> groupedList = new RealmList<>();
            groupedList.addAll(payments.subList(0, payments.size()));
            groupByDate.put(key, groupedList);
            key++;
        }
        return groupByDate;
    }

    public static HashMap<Integer, RealmList<Payment>> getPaymentsByDate(String query) {
        int month = getMonth(query);
        HashMap<Integer, RealmList<Payment>> groupedByDate = groupPaymentsByDate();
        HashMap<Integer, RealmList<Payment>> foundByDate = new HashMap<>();
        int newKey = 0;
        for (Map.Entry<Integer, RealmList<Payment>> entry : groupedByDate.entrySet()) {
            Date date = entry.getValue().get(0).getDate();
            if (date.getMonth() == month) {
                foundByDate.put(newKey++, entry.getValue());
            }
        }
        return foundByDate;
    }

    public static Price getPrices() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Price.class).findFirst();
    }
}
