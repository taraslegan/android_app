package com.example.payment.db;

import android.util.Log;

import com.example.payment.Utils;
import com.example.payment.model.Account;
import com.example.payment.model.Payment;
import com.example.payment.model.Price;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Тарас on 5/28/2018.
 */

public class DBInit {

    private static final double GAS_PRICE = 6.9579;
    private static final double WATER_PRICE = 5.46;
    private static final double LIGHT_PRICE = 0.9;

    public static void fillDb() {
        setAccounts();
        setPrices();
        setPayments();
    }

    private static void setAccounts() {
        Realm realm = Realm.getDefaultInstance();
        Account acc1 = new Account(1234567890L, 11204L, "gas");
        Account acc2 = new Account(9876543210L, 22088L, "light");
        Account acc3 = new Account(1234554321L, 11986L, "water");

        realm.executeTransaction(realm1 -> {
            realm1.insertOrUpdate(acc1);
            realm1.insertOrUpdate(acc2);
            realm1.insertOrUpdate(acc3);
        });
        List<Account> allAccounts = realm.where(Account.class).findAll();
        for (Account u : allAccounts) {
            Log.e("Accounts", u.toString());
        }

        List<Price> allPrices = realm.where(Price.class).findAll();
        for (Price p : allPrices) {
            Log.e("Prices", p.toString());
        }
    }

    private static void setPrices() {
        Price price = new Price(1, GAS_PRICE, WATER_PRICE, LIGHT_PRICE);
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(transaction -> realm.insertOrUpdate(price));

        List<Price> allPrices = realm.where(Price.class).findAll();
        for (Price p : allPrices) {
            Log.e("Prices", p.toString());
        }
    }

    private static void setPayments() {
        Realm realm = Realm.getDefaultInstance();

        Payment payment1 = new Payment(1, 1234567890L, 420, totalGasPrice(420), Utils.formatDate(new Date(1514757600000L)));
        Payment payment2 = new Payment(2, 9876543210L, 98, totalLightPrice(98), Utils.formatDate(new Date(1514757600000L)));
        Payment payment3 = new Payment(3, 1234554321L, 9, totalWaterPrice(7), Utils.formatDate(new Date(1514757600000L)));
        Payment payment4 = new Payment(4, 1234567890L, 407, totalGasPrice(407), Utils.formatDate(new Date(1517436000000L)));
        Payment payment5 = new Payment(5, 9876543210L, 95, totalLightPrice(95), Utils.formatDate(new Date(1517436000000L)));
        Payment payment6 = new Payment(6, 1234554321L, 8, totalWaterPrice(8), Utils.formatDate(new Date(1517436000000L)));
        Payment payment7 = new Payment(7, 1234567890L, 403, totalGasPrice(403), Utils.formatDate(new Date(1519855200000L)));
        Payment payment8 = new Payment(8, 9876543210L, 93, totalLightPrice(93), Utils.formatDate(new Date(1519855200000L)));
        Payment payment9 = new Payment(9, 1234554321L, 8, totalWaterPrice(8), Utils.formatDate(new Date(1519855200000L)));
        Payment payment10 = new Payment(10, 1234567890L, 133, totalGasPrice(133), Utils.formatDate(new Date(1522530000000L)));
        Payment payment11 = new Payment(11, 9876543210L, 65, totalLightPrice(65), Utils.formatDate(new Date(1522530000000L)));
        Payment payment12 = new Payment(12, 1234554321L, 7, totalWaterPrice(7), Utils.formatDate(new Date(1522530000000L)));
        Payment payment13 = new Payment(13, 1234567890L, 115, totalGasPrice(115), Utils.formatDate(new Date(1525122000000L)));
        Payment payment14 = new Payment(14, 9876543210L, 63, totalLightPrice(63), Utils.formatDate(new Date(1525122000000L)));
        Payment payment15 = new Payment(15, 1234554321L, 11, totalWaterPrice(11), Utils.formatDate(new Date(1525122000000L)));

        List<Payment> payments = Arrays.asList(payment1, payment2, payment3, payment4, payment5, payment6, payment7, payment8, payment9, payment10, payment11, payment12, payment13, payment14, payment15);

        realm.executeTransaction(transaction -> realm.insertOrUpdate(payments));

        List<Payment> allPayments = realm.where(Payment.class).findAll();
        for (Payment p : allPayments) {
            Log.e("Payments", p.toString());
        }
    }

    private static double totalGasPrice(int consumed) {
        Price prices = DBHelper.getPrices();
        return Math.round((prices.getGas() * consumed) * 100.0) / 100.0;
    }

    private static double totalLightPrice(int consumed) {
        Price prices = DBHelper.getPrices();
        return Math.round((prices.getLight() * consumed) * 100.0) / 100.0;
    }

    private static double totalWaterPrice(int consumed) {
        Price prices = DBHelper.getPrices();
        return Math.round((prices.getWater() * consumed) * 100.0) / 100.0;
    }

}
