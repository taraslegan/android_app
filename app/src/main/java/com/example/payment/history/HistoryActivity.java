package com.example.payment.history;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.payment.R;
import com.example.payment.custom_view.CustomSearchView;
import com.example.payment.db.DBHelper;
import com.example.payment.model.Payment;
import com.lapism.searchview.SearchView;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmList;

public class HistoryActivity extends AppCompatActivity {

    private Realm realm;
    private RecyclerView recyclerView;
    private HistoryAdapter historyAdapter;
    private CustomSearchView searchView;
    private static final int VOICE_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppThemeNoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setToolbar();
        initRecyclerView();
        initSearchView();
        realm = Realm.getDefaultInstance();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(true);
        historyAdapter = new HistoryAdapter(DBHelper.groupPaymentsByDate());
        recyclerView.setAdapter(historyAdapter);
    }

    private void initSearchView() {
        searchView = findViewById(R.id.search_view);
        searchView.setVoice(true, this);
        searchView.setHint("Пошук");
        searchView.setVersion(SearchView.VERSION_MENU_ITEM);
        searchView.setVersionMargins(SearchView.VERSION_MARGINS_MENU_ITEM);
        searchView.setShouldClearOnClose(false);
        searchView.setOnMenuClickListener(() -> searchView.close(true));
        searchView.setOnVoiceClickListener(() -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "ua");
            startActivityForResult(intent, VOICE_REQUEST_CODE);
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                HashMap<Integer, RealmList<Payment>> paymentsByDate = DBHelper.getPaymentsByDate(query);
                historyAdapter.clearData();
                historyAdapter.setData(paymentsByDate);
                historyAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case VOICE_REQUEST_CODE:
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchView.setQuery(text.get(0), true);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.history_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_button:
                searchView.open(true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
