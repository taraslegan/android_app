package com.example.payment.history;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.payment.R;
import com.example.payment.db.DBHelper;
import com.example.payment.model.Account;
import com.example.payment.model.Payment;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Тарас on 6/3/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private HashMap<Integer, RealmList<Payment>> data;

    public HistoryAdapter(HashMap<Integer, RealmList<Payment>> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context ctx = parent.getContext();
        View view = LayoutInflater.from(ctx).inflate(R.layout.card_view_history, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        if (null != data && data.size() != 0) {
            RealmList<Payment> payments = data.get(position);
            Date date = payments.get(0).getDate();
            Calendar instance = Calendar.getInstance();
            instance.setTime(date);

            holder.year.setText(instance.get(Calendar.YEAR) + "");
            holder.month.setText(DBHelper.getMonth(instance.get(Calendar.MONTH)));
            for (Payment p : payments) {
                Realm realm = Realm.getDefaultInstance();
                Account account = realm.where(Account.class).equalTo("ID_account", p.getID_account()).findFirst();
                switch (account.getAccount_type()) {
                    case "gas":
                        holder.tvGasUsed.setText(p.getConsumed_per_month() + " кубів");
                        holder.tvGasPrice.setText(p.getPrice() + " грн.");
                        break;
                    case "light":
                        holder.tvLightUsed.setText(p.getConsumed_per_month() + " кіловат");
                        holder.tvLightPrice.setText(p.getPrice() + " грн.");
                        break;
                    case "water":
                        holder.tvWaterUsed.setText(p.getConsumed_per_month() + " кубів");
                        holder.tvWaterPrice.setText(p.getPrice() + " грн.");
                        break;
                }
            }
        }
    }

    public void clearData() {
        data.clear();
    }

    public void setData(HashMap<Integer, RealmList<Payment>> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvGasUsed;
        TextView tvLightUsed;
        TextView tvWaterUsed;
        TextView tvGasPrice;
        TextView tvLightPrice;
        TextView tvWaterPrice;
        TextView year;
        TextView month;

        public HistoryViewHolder(View itemView) {
            super(itemView);
            tvGasUsed = itemView.findViewById(R.id.tv_gas_used);
            tvLightUsed = itemView.findViewById(R.id.tv_light_used);
            tvWaterUsed = itemView.findViewById(R.id.tv_water_used);
            tvGasPrice = itemView.findViewById(R.id.tv_gas_price);
            tvLightPrice = itemView.findViewById(R.id.tv_light_price);
            tvWaterPrice = itemView.findViewById(R.id.tv_water_price);
            year = itemView.findViewById(R.id.year);
            month = itemView.findViewById(R.id.month);
        }
    }
}
