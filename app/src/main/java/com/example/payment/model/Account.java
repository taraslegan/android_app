package com.example.payment.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Тарас on 14.05.2018.
 */

public class Account extends RealmObject {
    @PrimaryKey
    private long ID_account;
    private long counter_data;
    private String account_type; //3 account types: gas, water, light

    public Account() {}

    public Account(long ID_account, long counter_data, String account_type) {
        this.ID_account = ID_account;
        this.counter_data = counter_data;
        this.account_type = account_type;
    }

    public long getID_account() {
        return ID_account;
    }

    public void setID_account(long ID_account) {
        this.ID_account = ID_account;
    }

    public long getCounter_data() {
        return counter_data;
    }

    public void setCounter_data(long counter_data) {
        this.counter_data = counter_data;
    }

    public String getAccount_type() {
        return account_type;
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    @Override
    public String toString() {
        return "Account: id=[" + ID_account + "], " +
                "counter=[" + counter_data + "], " +
                "type=[" + account_type + "];";
    }
}
