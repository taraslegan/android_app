package com.example.payment.model;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Тарас on 14.05.2018.
 */

public class Payment extends RealmObject {
    @PrimaryKey
    private long ID_payment;
    private long ID_account;
    private int consumed_per_month;
    private double price;
    private Date date;

    public Payment() {
    }

    public Payment(long ID_account, int consumed_per_month, double price, Date date) {
        this.ID_account = ID_account;
        this.consumed_per_month = consumed_per_month;
        this.price = price;
        this.date = date;
    }

    public Payment(long ID_payment, long ID_account, int consumed_per_month, double price, Date date) {
        this(ID_account, consumed_per_month, price, date);
        this.ID_payment = ID_payment;
    }

    public long getID_payment() {
        return ID_payment;
    }

    public void setID_payment(long ID_payment) {
        this.ID_payment = ID_payment;
    }

    public long getID_account() {
        return ID_account;
    }

    public void setID_account(long ID_account) {
        this.ID_account = ID_account;
    }

    public int getConsumed_per_month() {
        return consumed_per_month;
    }

    public void setConsumed_per_month(int consumed_per_month) {
        this.consumed_per_month = consumed_per_month;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Payment: ID_payment=[" + ID_payment + "], ID_account=[" + ID_account + "], " +
                "consumed_per_month=[" + consumed_per_month + "], price=[" + price + "], " +
                "date=[" + date + "];";
    }
}