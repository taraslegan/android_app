package com.example.payment.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Тарас on 5/20/2018.
 */

public class Price extends RealmObject {
    @PrimaryKey
    private int ID_price;
    private double gas;
    private double water;
    private double light;

    public Price(){}

    public Price(int ID_price, double gas, double water, double light) {
        this.ID_price = ID_price;
        this.gas = gas;
        this.water = water;
        this.light = light;
    }

    public int getID_price() {
        return ID_price;
    }

    public double getGas() {
        return gas;
    }

    public double getWater() {
        return water;
    }

    public double getLight() {
        return light;
    }

    @Override
    public String toString() {
        return "Price: id = [" + ID_price + "], " +
                "gas = [" + gas + "], " +
                "water = [" + water + "], " +
                "light = [" + light + "];";
    }
}
