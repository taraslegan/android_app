package com.example.payment.tabs;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.payment.Utils;
import com.example.payment.db.DBHelper;
import com.example.payment.model.Account;
import com.example.payment.model.Payment;
import com.example.payment.model.Price;

import java.util.Date;

import io.realm.Realm;

/**
 * Created by Тарас on 6/3/2018.
 */

public class TabHelper {

    private void confirmAndDisplayPay(EditText etCounterData, TextView tvLastCounter, TextView tvConsumedPerMonth, Activity activity, String tabIdentifier) {
        Realm realm = Realm.getDefaultInstance();

        long newCounterData = Long.valueOf(etCounterData.getText().toString().trim());

        int[] consumedPerMonth = new int[1];

        realm.executeTransaction(transaction -> {
            Account accountToUpdate = DBHelper.getAccount(activity, tabIdentifier);
            long oldCounterData = accountToUpdate.getCounter_data();
            double utilityPrice = getPrice(tabIdentifier);
            accountToUpdate.setCounter_data(newCounterData);
            consumedPerMonth[0] = (int) (newCounterData - oldCounterData);
            double totalPrice = consumedPerMonth[0] * utilityPrice;
            double roundedTotalPrice = Math.round(totalPrice * 100.0) / 100.0;
            Payment payment = new Payment(accountToUpdate.getID_account(),
                    consumedPerMonth[0], roundedTotalPrice, Utils.formatDate(new Date()));
            realm.insertOrUpdate(accountToUpdate);
            saveWithAutoincrementKey(payment);
        });

        tvLastCounter.setText(String.valueOf(newCounterData));
        tvConsumedPerMonth.setText(String.valueOf(consumedPerMonth[0]));

        realm.close();
    }

    public void tryPay(EditText etCounterData, TextView tvLastCounter, TextView tvConsumedPerMonth, Activity activity, String tabIdentifier) {
        Date lastPaidMonth = DBHelper.getLastPaidMonth(activity, tabIdentifier);
        if (Utils.formatDate(new Date()).equals(lastPaidMonth)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(activity)
                    .setTitle("Ви вже оплатили за " + DBHelper.getMonth(lastPaidMonth.getMonth()) + "!")
                    .setPositiveButton("OK", (dialog, which) -> {
                        etCounterData.setText("");
                    });
            alert.show();
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(activity)
                    .setTitle("Операцію не можна відмінити, всеодно оплатити?")
                    .setPositiveButton("Оплатити", (dialog, which) -> {
                        confirmAndDisplayPay(etCounterData, tvLastCounter, tvConsumedPerMonth, activity, tabIdentifier);
                        Toast.makeText(activity, "Успішно оплачено за " + DBHelper.getMonth(new Date().getMonth()) + "!", Toast.LENGTH_LONG).show();
                        etCounterData.setText("");
                    })
                    .setNegativeButton("Скасувати", null);
            alert.show();
        }
    }

    private void saveWithAutoincrementKey(Payment payment) {
        Realm realm = Realm.getDefaultInstance();
        Number paymentId = realm.where(Payment.class).max("ID_payment");
        if (paymentId == null) payment.setID_payment(1);
        else payment.setID_payment(paymentId.intValue() + 1);
        realm.insertOrUpdate(payment);
        realm.close();
    }

    public boolean validateCounterData(TextInputLayout etCurrentCounterLayout, EditText etCounterData, Activity activity, String tabIdentifier) {
        if ("".equals(etCounterData.getText().toString())) {
            etCurrentCounterLayout.setError("Введіть показник!");
            etCurrentCounterLayout.setErrorEnabled(true);
            return false;
        } else if (Long.valueOf(etCounterData.getText().toString()) < DBHelper.getAccount(activity, tabIdentifier).getCounter_data()) {
            etCurrentCounterLayout.setError("Показник лічильника не може бути меншим за попередній!");
            etCurrentCounterLayout.setErrorEnabled(true);
            return false;
        } else {
            etCurrentCounterLayout.setErrorEnabled(false);
            return true;
        }
    }

    private double getPrice(String tabIdentifier) {
        Realm realm = Realm.getDefaultInstance();
        Price prices = realm.where(Price.class).findFirst();
        double price = 0;
        switch (tabIdentifier) {
            case "gas":
                price = prices.getGas();
                break;
            case "light":
                price = prices.getLight();
                break;
            case "water":
                price = prices.getWater();
                break;
        }
        return price;
    }


}
