package com.example.payment.tabs;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.payment.R;

/**
 * Created by Тарас on 01.05.2018.
 */

public class UtilitiesFragmentAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public UtilitiesFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new GasTabFragment();
        } else if (position == 1){
            return new LightTabFragment();
        } else  {
            return new WaterTabFragment();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.tab_gas);
            case 1:
                return mContext.getString(R.string.tab_light);
            case 2:
                return mContext.getString(R.string.tab_water);
            default:
                return null;
        }
    }
}
