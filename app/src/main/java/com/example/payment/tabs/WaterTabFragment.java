package com.example.payment.tabs;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.R;
import com.example.payment.SaveSharedPreferences;
import com.example.payment.db.DBHelper;
import com.example.payment.model.Account;
import com.example.payment.model.Payment;

import java.util.List;

import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class WaterTabFragment extends Fragment implements View.OnClickListener{

    private EditText etCounterData;
    private TextView tvLastCounter;
    private TextView tvConsumedPerMonth;
    private TextInputLayout etCurrentCounterLayout;
    private TextView tvWaterPriceInfo;
    private Realm realm;
    private String tabIdentifier = "water";

    public WaterTabFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_water_tab, container, false);
        realm = Realm.getDefaultInstance();
        etCounterData = rootView.findViewById(R.id.et_current_counter);
        tvLastCounter = rootView.findViewById(R.id.tv_last_counter_data);
        tvConsumedPerMonth = rootView.findViewById(R.id.tv_used_utility);
        etCurrentCounterLayout = rootView.findViewById(R.id.et_current_counter_layout);
        tvWaterPriceInfo = rootView.findViewById(R.id.tv_water_price_info);
        rootView.findViewById(R.id.pay).setOnClickListener(this);

        tvWaterPriceInfo.setText("Ціна за 1 куб: " + DBHelper.getPrices().getWater() + " грн.");
        tvLastCounter.setText(String.valueOf(DBHelper.getLastCounterData(getActivity(), tabIdentifier)));
        tvConsumedPerMonth.setText(String.valueOf(DBHelper.getLastConsumedPerMonth(getActivity(), tabIdentifier)));
        return rootView;
    }

    @Override
    public void onClick(View v) {
        TabHelper tabHelper = new TabHelper();
        if (tabHelper.validateCounterData(etCurrentCounterLayout, etCounterData, getActivity(), tabIdentifier)) {
            tabHelper.tryPay(etCounterData, tvLastCounter, tvConsumedPerMonth, getActivity(), tabIdentifier);

            List<Account> allAccounts = realm.where(Account.class).findAll();
            for (Account u : allAccounts) {
                Log.e("Accounts", u.toString());
            }

            List<Payment> allPayments = realm.where(Payment.class).findAll();
            for (Payment p : allPayments) {
                Log.e("Payments", p.toString());
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
}
