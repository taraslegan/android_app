package com.example.payment.user_registration;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.payment.LogInActivity;
import com.example.payment.R;
import com.example.payment.model.User;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;


/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordFragment extends Fragment implements View.OnClickListener{

    EditText etPassword;
    TextView tvInvalidPassword;

    public PasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_password, container, false);
        etPassword = rootView.findViewById(R.id.password_registration);
        tvInvalidPassword = rootView.findViewById(R.id.invalid_password);
        rootView.findViewById(R.id.next_button).setOnClickListener(this);
        return rootView;
    }
    
    private void saveToRealm(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
                    Number userId = realm1.where(User.class).max("ID_user");
                    if (userId == null) user.setID_user(1);
                    else user.setID_user(userId.intValue() + 1);
                    realm1.insertOrUpdate(user);
        }
        );
        List<User> allUsers = realm.where(User.class).findAll();
        for (User u : allUsers) {
            Log.e("Users", u.toString());
        }
        realm.close();
    }

    @Override
    public void onClick(View v) {
        String userName = getArguments().getString("user_name");
        String password = etPassword.getText().toString();
        if (!validatePassword(password)) {
            tvInvalidPassword.setVisibility(View.VISIBLE);
            return;
        }
        tvInvalidPassword.setVisibility(View.INVISIBLE);
        User user = new User();
        user.setID_user(1);
        user.setUser_name(userName);
        user.setPassword(password);
        saveToRealm(user);
        Toast.makeText(getActivity().getApplicationContext(), R.string.registered_successfully, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity().getApplicationContext(), LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    private boolean validatePassword(String password) {
        Pattern regex = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$");
        Matcher matcher = regex.matcher(password);
        return matcher.matches();
    }
}
