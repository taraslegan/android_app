package com.example.payment.user_registration;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.example.payment.R;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        Fragment userNameFragment = new UserNameFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.registration_layout, userNameFragment, userNameFragment.getClass().getSimpleName()).commit();
    }

}
