package com.example.payment.user_registration;


import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.R;
import com.example.payment.model.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;


/**
 * A simple {@link Fragment} subclass.
 */
public class UserNameFragment extends Fragment implements View.OnClickListener {

    private TextView tvNoSuchUser;
    private TextInputLayout userNameLayout;
    private EditText etUserName;
    private Button nextButton;

    public UserNameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_name, container, false);
        nextButton = rootView.findViewById(R.id.next_button);
        etUserName = rootView.findViewById(R.id.user_name_registration);
        tvNoSuchUser = rootView.findViewById(R.id.no_user);
        nextButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        String etUserNameValue = etUserName.getText().toString();
        if (isSuchUserNameExists(etUserNameValue)) {
            tvNoSuchUser.setText(R.string.such_user_exists);
            tvNoSuchUser.setVisibility(View.VISIBLE);
            return;
        }
        if (!validateUserName(etUserNameValue)) processWrongUserName(etUserNameValue);
        else {
            tvNoSuchUser.setVisibility(View.INVISIBLE);

            Bundle bundle = new Bundle();
            bundle.putString("user_name", etUserNameValue);

            Fragment passwordFragment = new PasswordFragment();
            passwordFragment.setArguments(bundle);

            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.registration_layout, passwordFragment, passwordFragment.getClass().getSimpleName())
                    .addToBackStack("passwordFragment").commit();
        }
    }

    private boolean validateUserName(String userName) {
        Pattern regex = Pattern.compile("^[A-Za-z0-9_-]{3,15}");
        Matcher matcher = regex.matcher(userName);
        return matcher.matches();
    }

    private void processWrongUserName(String userName) {
        if (userName.length() >= 3 && userName.length() <= 15)
            tvNoSuchUser.setText(R.string.forbidden_symbols_user_name);
        if (userName.length() < 3)
            tvNoSuchUser.setText(R.string.short_user_name);
        else if (userName.length() > 15)
            tvNoSuchUser.setText(R.string.long_user_name);
        tvNoSuchUser.setVisibility(View.VISIBLE);
    }

    private boolean isSuchUserNameExists(String userName) {
        Realm realm = Realm.getDefaultInstance();
        User user = realm.where(User.class).equalTo("user_name", userName).findFirst();
        realm.close();
        return user != null;
    }

}
