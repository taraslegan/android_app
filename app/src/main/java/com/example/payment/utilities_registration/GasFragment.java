package com.example.payment.utilities_registration;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.R;
import com.example.payment.SaveSharedPreferences;
import com.example.payment.model.Account;

import io.realm.Realm;


/**
 * A simple {@link Fragment} subclass.
 */
public class GasFragment extends Fragment implements View.OnClickListener {

    private EditText etGasId;
    private TextView tvInvalidGasAccount;
    public GasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_gas, container, false);
        rootView.findViewById(R.id.next_button).setOnClickListener(this);
        etGasId = rootView.findViewById(R.id.et_id_gas);
        tvInvalidGasAccount = rootView.findViewById(R.id.invalid_gas_account);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (etGasId.getText().toString().length() != 10) {
            tvInvalidGasAccount.setText(R.string.invalid_account_length);
            tvInvalidGasAccount.setVisibility(View.VISIBLE);
            return;
        }
        Long gasAccount = Long.valueOf(etGasId.getText().toString());
        Realm realm = Realm.getDefaultInstance();
        Account foundAccount = realm.where(Account.class).equalTo("ID_account", gasAccount)
                .equalTo("account_type", "gas").findFirst();
        if (foundAccount == null) {
            tvInvalidGasAccount.setText(R.string.invalid_account);
            tvInvalidGasAccount.setVisibility(View.VISIBLE);
        } else {
            tvInvalidGasAccount.setVisibility(View.INVISIBLE);
            SaveSharedPreferences.setGasId(getActivity(), etGasId.getText().toString());
            Fragment lightFragment = new LightFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.utilities_container, lightFragment, lightFragment.getClass().getSimpleName())
                    .addToBackStack("lightFragment").commit();
        }
    }
}
