package com.example.payment.utilities_registration;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.R;
import com.example.payment.SaveSharedPreferences;
import com.example.payment.model.Account;

import io.realm.Realm;


/**
 * A simple {@link Fragment} subclass.
 */
public class LightFragment extends Fragment implements View.OnClickListener {

    private EditText etLightId;
    private TextView tvInvalidLightAccount;

    public LightFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_light, container, false);
        rootView.findViewById(R.id.next_button).setOnClickListener(this);
        etLightId = rootView.findViewById(R.id.et_id_light);
        tvInvalidLightAccount = rootView.findViewById(R.id.invalid_light_account);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (etLightId.getText().toString().length() != 10) {
            tvInvalidLightAccount.setText(R.string.invalid_account_length);
            tvInvalidLightAccount.setVisibility(View.VISIBLE);
            return;
        }
        Long lightAccount = Long.valueOf(etLightId.getText().toString());
        Realm realm = Realm.getDefaultInstance();
        Account foundAccount = realm.where(Account.class).equalTo("ID_account", lightAccount)
                .equalTo("account_type", "light").findFirst();
        if (foundAccount == null) {
            tvInvalidLightAccount.setText(R.string.invalid_account);
            tvInvalidLightAccount.setVisibility(View.VISIBLE);
        } else {
            tvInvalidLightAccount.setVisibility(View.INVISIBLE);
            SaveSharedPreferences.setLightId(getActivity(), etLightId.getText().toString());
            Fragment waterFragment = new WaterFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.utilities_container, waterFragment, waterFragment.getClass().getSimpleName())
                    .addToBackStack("waterFragment").commit();
        }
    }
}
