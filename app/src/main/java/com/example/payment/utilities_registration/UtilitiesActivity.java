package com.example.payment.utilities_registration;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.payment.R;

public class UtilitiesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utilities);

        Fragment gasFragment = new GasFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.utilities_container, gasFragment, gasFragment.getClass().getSimpleName()).commit();
    }
}
