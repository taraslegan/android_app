package com.example.payment.utilities_registration;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.payment.MainActivity;
import com.example.payment.R;
import com.example.payment.SaveSharedPreferences;
import com.example.payment.model.Account;

import io.realm.Realm;


/**
 * A simple {@link Fragment} subclass.
 */
public class WaterFragment extends Fragment implements View.OnClickListener {

    private EditText etWaterId;
    private TextView tvInvalidWaterAccount;


    public WaterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_water, container, false);
        etWaterId = rootView.findViewById(R.id.et_id_water);
        tvInvalidWaterAccount = rootView.findViewById(R.id.invalid_water_account);
        rootView.findViewById(R.id.next_button).setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (etWaterId.getText().toString().length() != 10) {
            tvInvalidWaterAccount.setText(R.string.invalid_account_length);
            tvInvalidWaterAccount.setVisibility(View.VISIBLE);
            return;
        }

        Long waterAccount = Long.valueOf(etWaterId.getText().toString());
        Realm realm = Realm.getDefaultInstance();
        Account foundAccount = realm.where(Account.class).equalTo("ID_account", waterAccount)
                .equalTo("account_type", "water").findFirst();
        if (foundAccount == null) {
            tvInvalidWaterAccount.setText(R.string.invalid_account);
            tvInvalidWaterAccount.setVisibility(View.VISIBLE);
        } else {
            tvInvalidWaterAccount.setVisibility(View.INVISIBLE);
            SaveSharedPreferences.setWaterId(getActivity(), etWaterId.getText().toString());
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
}
